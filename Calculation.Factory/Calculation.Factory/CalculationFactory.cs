﻿using Calculation.Services.Division;
using Calculation.Services.Multiplication;
using Calculation.Services.Subtraction;
using Calculation.Services.Sum;

namespace Calculation.Factory
{
    public class CalculationFactory : ICalculationFactory
    {
        private IDivisionService _divisionService;
        public IDivisionService DivisionService
        {
            get { return _divisionService ?? (_divisionService = new DivisionService()); }
        }

        private IMultiplicationService _multiplicationService;
        public IMultiplicationService MultiplicationService
        {
            get { return _multiplicationService ?? (_multiplicationService = new MultiplicationService()); }
        }

        private ISubtractionService _subtractionService;
        public ISubtractionService SubtractionService
        {
            get { return _subtractionService ?? (_subtractionService = new SubtractionService()); }
        }

        private ISumService _sumService;
        public ISumService SumService
        {
            get { return _sumService ?? (_sumService = new SumService()); }
        }
    }
}