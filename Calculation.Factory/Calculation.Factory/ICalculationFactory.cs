﻿using Calculation.Services.Division;
using Calculation.Services.Multiplication;
using Calculation.Services.Subtraction;
using Calculation.Services.Sum;

namespace Calculation.Factory
{
    public interface ICalculationFactory
    {
        IDivisionService DivisionService { get; }
        IMultiplicationService MultiplicationService { get; }
        ISubtractionService SubtractionService { get; }
        ISumService SumService { get; }
    }
}