﻿using Calculation.Domain;

namespace Calculation.Services.Sum
{
    public class SumService : ISumService
    {
        public Fraction Sum(Fraction firstArg, Fraction secondArg)
        {
            if (firstArg.Denominator == secondArg.Denominator)
                return new Fraction
                {
                    Numerator = firstArg.Numerator + secondArg.Numerator,
                    Denominator = firstArg.Denominator
                };

            var firstNumerator = firstArg.Numerator * secondArg.Denominator;
            var secondNumerator = secondArg.Numerator * firstArg.Denominator;

            return new Fraction
            {
                Denominator = firstArg.Denominator * secondArg.Denominator,
                Numerator = firstNumerator + secondNumerator
            };
        }

        public int Sum(int firstArg, int secondArg)
        {
            return firstArg + secondArg;
        }

        public double Sum(double firstArg, double secondArg)
        {
            return firstArg + secondArg;
        }

        public double Sum(int firstArg, double secondArg)
        {
            return firstArg + secondArg;
        }

        public double Sum(double firstArg, int secondArg)
        {
            return firstArg + secondArg;
        }
    }
}