﻿using Calculation.Domain;

namespace Calculation.Services.Sum
{
    public interface ISumService
    {
        Fraction Sum(Fraction firstArg, Fraction secondArg);
        int Sum(int firstArg, int secondArg);
        double Sum(double firstArg, double secondArg);
        double Sum(int firstArg, double secondArg);
        double Sum(double firstArg, int secondArg);
    }
}