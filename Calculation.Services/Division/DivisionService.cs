﻿using System;
using Calculation.Domain;

namespace Calculation.Services.Division
{
    public class DivisionService : IDivisionService
    {
        public Fraction Division(Fraction firstArg, Fraction secondArg)
        {
            return new Fraction
            {
                Numerator = firstArg.Numerator * secondArg.Denominator,
                Denominator = firstArg.Denominator * secondArg.Numerator
            };
        }

        public double Division(int firstArg, int secondArg)
        {
            var secondDoubleArg = (double)secondArg;
            CheckArgumentForZeroValue(secondDoubleArg);
            return firstArg / secondDoubleArg;
        }

        public double Division(double firstArg, double secondArg)
        {
            CheckArgumentForZeroValue(secondArg);
            return firstArg / secondArg;
        }

        public double Division(double firstArg, int secondArg)
        {
            CheckArgumentForZeroValue(secondArg);
            return firstArg / secondArg;
        }

        public double Division(int firstArg, double secondArg)
        {
            CheckArgumentForZeroValue(secondArg);
            return firstArg / secondArg;
        }

        private static void CheckArgumentForZeroValue(double argument)
        {
            if (Math.Abs(argument - 0) <= 0)
                throw new DivideByZeroException();
        }
    }
}