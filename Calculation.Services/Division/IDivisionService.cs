﻿using Calculation.Domain;

namespace Calculation.Services.Division
{
    public interface IDivisionService
    {
        Fraction Division(Fraction firstArg, Fraction secondArg);
        double Division(int firstArg, int secondArg);
        double Division(double firstArg, double secondArg);
        double Division(double firstArg, int secondArg);
        double Division(int firstArg, double secondArg);
    }
}