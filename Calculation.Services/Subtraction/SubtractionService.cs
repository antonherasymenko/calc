﻿using Calculation.Domain;

namespace Calculation.Services.Subtraction
{
    public class SubtractionService : ISubtractionService
    {
        public Fraction Subtraction(Fraction firstArg, Fraction secondArg)
        {
            if (firstArg.Denominator== secondArg.Denominator)
                return new Fraction{Numerator = firstArg.Numerator - secondArg.Numerator, Denominator = firstArg.Denominator};

            var firstNumerator = firstArg.Numerator * secondArg.Denominator;
            var secondNumerator = secondArg.Numerator * firstArg.Denominator;

            return new Fraction
            {
                Numerator = firstNumerator - secondNumerator,
                Denominator = firstArg.Denominator * secondArg.Denominator
            };

        }

        public int Subtraction(int firstArg, int secondArg)
        {
            return firstArg - secondArg;
        }

        public double Subtraction(double firstArg, double secondArg)
        {
            return ConvertToDouble((decimal)(firstArg - secondArg));
        }

        public double Subtraction(int firstArg, double secondArg)
        {
            return ConvertToDouble((decimal)(firstArg - secondArg));
        }

        public double Subtraction(double firstArg, int secondArg)
        {
            return ConvertToDouble((decimal)(firstArg - secondArg));
        }

        private static double ConvertToDouble(decimal arg)
        {
            return (double)arg;
        }
    }
}