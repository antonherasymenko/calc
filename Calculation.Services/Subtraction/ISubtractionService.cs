﻿using Calculation.Domain;

namespace Calculation.Services.Subtraction
{
    public interface ISubtractionService
    {
        Fraction Subtraction(Fraction firstArg, Fraction secondArg);
        int Subtraction(int firstArg, int secondArg);
        double Subtraction(double firstArg, double secondArg);
        double Subtraction(int firstArg, double secondArg);
        double Subtraction(double firstArg, int secondArg);
    }
}