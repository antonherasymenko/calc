﻿using Calculation.Domain;

namespace Calculation.Services.Multiplication
{
    public class MultiplicationService : IMultiplicationService
    {
        public Fraction Multiplication(Fraction firstArg, Fraction secondArg)
        {
            return new Fraction
            {
                Numerator = firstArg.Numerator * secondArg.Numerator,
                Denominator = firstArg.Denominator * secondArg.Denominator
            };
        }

        public int Multiplication(int firstArg, int secondArg)
        {
            return firstArg * secondArg;
        }

        public double Multiplication(double firstArg, double secondArg)
        {
            return firstArg * secondArg;
        }

        public double Multiplication(double firstArg, int secondArg)
        {
            return firstArg * secondArg;
        }

        public double Multiplication(int firstArg, double secondArg)
        {
            return firstArg * secondArg;
        }
    }
}