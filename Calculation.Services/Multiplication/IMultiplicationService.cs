﻿using Calculation.Domain;

namespace Calculation.Services.Multiplication
{
    public interface IMultiplicationService
    {
        Fraction Multiplication(Fraction firstArg, Fraction secondArg);
        int Multiplication(int firstArg, int secondArg);
        double Multiplication(double firstArg, double secondArg);
        double Multiplication(double firstArg, int secondArg);
        double Multiplication(int firstArg, double secondArg);
    }
}