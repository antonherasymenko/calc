﻿using Calculation.Domain;
using Calculation.Factory;

namespace Calculation
{
    public static class Calculator
    {
        private static readonly ICalculationFactory CalculationFactory = new CalculationFactory();

        #region Division

        public static Fraction Division(Fraction firstArg, Fraction secondArg)
        {
            return CalculationFactory.DivisionService.Division(firstArg, secondArg);
        }

        public static double Division(int firstArg, int secondArg)
        {
            return CalculationFactory.DivisionService.Division(firstArg, secondArg);
        }

        public static double Division(double firstArg, double secondArg)
        {
            return CalculationFactory.DivisionService.Division(firstArg, secondArg);
        }

        public static double Division(double firstArg, int secondArg)
        {
            return CalculationFactory.DivisionService.Division(firstArg, secondArg);
        }

        public static double Division(int firstArg, double secondArg)
        {
            return CalculationFactory.DivisionService.Division(firstArg, secondArg);
        }
        #endregion

        #region Multiplication

        public static Fraction Multiplication(Fraction firstArg, Fraction secondArg)
        {
            return CalculationFactory.MultiplicationService.Multiplication(firstArg, secondArg);
        }

        public static int Multiplication(int firstArg, int secondArg)
        {
            return CalculationFactory.MultiplicationService.Multiplication(firstArg, secondArg);
        }

        public static double Multiplication(double firstArg, double secondArg)
        {
            return CalculationFactory.MultiplicationService.Multiplication(firstArg, secondArg);
        }

        public static double Multiplication(double firstArg, int secondArg)
        {
            return CalculationFactory.MultiplicationService.Multiplication(firstArg, secondArg);
        }
        
        public static double Multiplication(int firstArg, double secondArg)
        {
            return CalculationFactory.MultiplicationService.Multiplication(firstArg, secondArg);
        }
        #endregion

        #region Subtraction

        public static Fraction Subtraction(Fraction firstArg, Fraction secondArg)
        {
            return CalculationFactory.SubtractionService.Subtraction(firstArg, secondArg);
        }

        public static int Subtraction(int firstArg, int secondArg)
        {
            return CalculationFactory.SubtractionService.Subtraction(firstArg, secondArg);
        }

        public static double Subtraction(double firstArg, double secondArg)
        {
            return CalculationFactory.SubtractionService.Subtraction(firstArg, secondArg);
        }

        public static double Subtraction(int firstArg, double secondArg)
        {
            return CalculationFactory.SubtractionService.Subtraction(firstArg, secondArg);
        }

        public static double Subtraction(double firstArg, int secondArg)
        {
            return CalculationFactory.SubtractionService.Subtraction(firstArg, secondArg);
        }
        #endregion

        #region Sum
        public static Fraction Sum(Fraction firstArg, Fraction secondArg)
        {
            return CalculationFactory.SumService.Sum(firstArg, secondArg);
        }

        public static int Sum(int firstArg, int secondArg)
        {
            return CalculationFactory.SumService.Sum(firstArg, secondArg);
        }

        public static double Sum(double firstArg, double secondArg)
        {
            return CalculationFactory.SumService.Sum(firstArg, secondArg);
        }

        public static double Sum(int firstArg, double secondArg)
        {
            return CalculationFactory.SumService.Sum(firstArg, secondArg);
        }

        public static double Sum(double firstArg, int secondArg)
        {
            return CalculationFactory.SumService.Sum(firstArg, secondArg);
        }
        #endregion
    }
}