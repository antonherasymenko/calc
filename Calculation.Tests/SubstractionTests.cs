﻿using Calculation.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculation.Tests
{
    [TestClass]
    public class SubstractionTests
    {
        [TestMethod]
        public void TestSubtractionWithFractionWithSameDenomenator()
        {
            Assert.AreEqual(new Fraction(-1,10), Calculator.Subtraction(new Fraction(3,10), new Fraction(4,10)));
        }
        
        [TestMethod]
        public void TestSubtractionWithFractionWithDifferentDenomenator()
        {
            Assert.AreEqual(new Fraction(-1,63), Calculator.Subtraction(new Fraction(3,7), new Fraction(4,9)));
        }

        [TestMethod]
        public void TestSubstractionWithIntAndInt()
        {
            Assert.AreEqual(-1, Calculator.Subtraction(5,6));
        }

        [TestMethod]
        public void TestSubstractionWithDoubleAndDouble()
        {
            Assert.AreEqual(1.5, Calculator.Subtraction(5.2, 3.7));
        }

        [TestMethod]
        public void TestSubstractionWithIntAndDouble()
        {
            Assert.AreEqual(3.5, Calculator.Subtraction(5, 1.5));
        }

        [TestMethod]
        public void TestSubstractionWithDoubleAndInt()
        {
            Assert.AreEqual(-0.7, Calculator.Subtraction(5.3, 6));
        }
    }
}