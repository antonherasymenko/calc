﻿using Calculation.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculation.Tests
{
    [TestClass]
    public class SumTests
    {
        [TestMethod]
        public void TestSumWithFractionWithSameDenomenator()
        {
            Assert.AreEqual(new Fraction(7, 10), Calculator.Sum(new Fraction(3, 10), new Fraction(4, 10)));
        }

        [TestMethod]
        public void TestSumWithFractionWithDifferentDenomenator()
        {
            Assert.AreEqual(new Fraction(55, 63), Calculator.Sum(new Fraction(3, 7), new Fraction(4, 9)));
        }

        [TestMethod]
        public void TestSumWithIntAndInt()
        {
            Assert.AreEqual(3, Calculator.Sum(1, 2));
        }

        [TestMethod]
        public void TestSumWithIntAndDouble()
        {
            Assert.AreEqual(3.5, Calculator.Sum(2, 1.5));
        }

        [TestMethod]
        public void TestSumWithDoubleAndDouble()
        {
            Assert.AreEqual(4.2, Calculator.Sum(3.1, 1.1));
        }

        [TestMethod]
        public void TestSumWithDoubleAndInt()
        {
            Assert.AreEqual(5.3, Calculator.Sum(3.3, 2));
        }
         
    }
}