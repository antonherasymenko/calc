﻿using System;
using Calculation.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculation.Tests
{
    [TestClass]
    public class DivisionTests
    {
        [TestMethod]
        public void TestDivisionWithFractions()
        {
            Assert.AreEqual(new Fraction(21, 10), Calculator.Division(new Fraction(3, 5), new Fraction(2, 7)));
        }

        [TestMethod]
        public void TestDivisionWithIntAndInt()
        {
            Assert.AreEqual(3, Calculator.Division(6, 2));
        }

        [TestMethod]
        public void TestDivisionWithIntAndDouble()
        {
            Assert.AreEqual(4, Calculator.Division(6, 1.5));
        }

        [TestMethod]
        public void TestDivisionWithDoubleAndInt()
        {
            Assert.AreEqual(0.6, Calculator.Division(6.6, 11));
        }

        [TestMethod]
        public void TestDivisionWithDoubleAndDouble()
        {
            Assert.AreEqual(6.875, Calculator.Division(5.5, 0.8));
        }

        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void TestDivistionWithZero()
        {
            Calculator.Division(5, 0);

        }
    }
}