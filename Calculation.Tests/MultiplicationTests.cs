﻿using Calculation.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Calculation.Tests
{
    [TestClass]
    public class MultiplicationTests
    {
        [TestMethod]
        public void TestMultiplicaitonWithFractions()
        {
            Assert.AreEqual(new Fraction(12,30), Calculator.Multiplication(new Fraction(3, 5), new Fraction(4,6)));
        }

        [TestMethod]
        public void TestMultiplicationWithIntAndInt()
        {
            Assert.AreEqual(24, Calculator.Multiplication(3,8));
        }

        [TestMethod]
        public void TestMultiplicationWithIntAndDouble()
        {
            Assert.AreEqual(37.1, Calculator.Multiplication(7, 5.3));
        }

        [TestMethod]
        public void TestMultiplicationWithDoubleAndInt()
        {
            Assert.AreEqual(-37.1, Calculator.Multiplication(5.3, -7));
        }

        [TestMethod]
        public void TestMultiplicationWithDoubleAndDouble()
        {
            Assert.AreEqual(16.01166, Calculator.Multiplication(4.2, 3.8123));
        }
    }
}