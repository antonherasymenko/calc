﻿namespace Calculation.Domain
{
    public struct Fraction
    {
        public Fraction(int numerator, int denumerator) : this()
        {
            Numerator = numerator;
            Denominator = denumerator;
        }

        public int Numerator { get; set; }
        public int Denominator { get; set; }
    }
}